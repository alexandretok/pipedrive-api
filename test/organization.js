const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const should = chai.should();
const models = require('../models');

chai.use(chaiHttp);

describe('Organizations', () => {
  // Clear the test database before
  before(done => {
    models.sequelize.sync().then(() => {
      models.Organization.destroy({where: {}}).then(() => {
        done();
      })
    });
  });

  // Wait a little bit before calling next test
  afterEach(done => {
    setTimeout(() => {
      done();
    }, 500);
  });

  /*
  * Test the /POST route
  */
  describe('/POST organizations', () => {
    it('it should POST organizations', (done) => {
      let organizations = {
        "org_name": "Paradise Island",
        "daughters": [
          {
            "org_name": "Banana tree",
            "daughters": [
              {"org_name": "Yellow Banana"},
              {"org_name": "Brown Banana"},
              {"org_name": "Black Banana"},
            ]
          },
          {
            "org_name": "Big banana tree",
            "daughters": [
              {"org_name": "Yellow Banana"},
              {"org_name": "Brown Banana"},
              {"org_name": "Green Banana"},
              {
                "org_name": "Black Banana",
                "daughters": [
                  {"org_name": "Phoneutria Spider"}
                ]
              },
            ]
          },
        ]
      };

      chai.request(server)
        .post('/organizations')
        .send(organizations)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });

  /*
  * Test the /GET route
  */
  describe('/GET organization', () => {
    it('it should GET all organizations', (done) => {
      chai.request(server)
        .get('/organizations/Black%20Banana')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(6);

          res.body[0].org_name.should.be.eql('Banana tree');
          res.body[0].relationship_type.should.be.eql('parent');
          
          res.body[1].org_name.should.be.eql('Big banana tree');
          res.body[1].relationship_type.should.be.eql('parent');

          res.body[2].org_name.should.be.eql('Brown Banana');
          res.body[2].relationship_type.should.be.eql('sister');

          res.body[3].org_name.should.be.eql('Green Banana');
          res.body[3].relationship_type.should.be.eql('sister');

          res.body[4].org_name.should.be.eql('Phoneutria Spider');
          res.body[4].relationship_type.should.be.eql('daughter');

          res.body[5].org_name.should.be.eql('Yellow Banana');
          res.body[5].relationship_type.should.be.eql('sister');

          done();
        });
    });
  });

});
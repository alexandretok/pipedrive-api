# Pipedrive - Organizations API Test

This application was developed using:

* NodeJS
    * Express Framework
    * Sequelize ORM
    * Mocha
    * Chai
* MySQL
* Docker

The API has two endpoints, as requested in the requirements:

**GET** `http://localhost:3000/organizations/:org_name/:page?`

AND

**POST** `http://localhost:3000/organizations`
```
{
  "org_name": "Paradise Island",
  "daughters": [
    ...
  ]
}
```

If you have **Docker** installed, just run `docker-compose up`.

**If not**, configure your MySQL connection on `config/config.json` and run the following commands to start or test the application.

* `npm start`

* `npm test`
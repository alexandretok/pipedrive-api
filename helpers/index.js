const helper = {
  refactorOrganizations: (organization, array = []) => {
    if(!array.length)
      // Top organization has no parents
      array.push({
        org_name: organization.org_name,
        parents: []
      });

    if(organization.daughters && organization.daughters.length) {
      // If has daughters
      organization.daughters.forEach(daughter => {
        let exists = array.findIndex(org => org.org_name === daughter.org_name);
        if(exists > -1){
          // If daughter already exists on array
          array[exists].parents.push(organization.org_name);
        } else {
          array.push({
            org_name: daughter.org_name,
            parents: [organization.org_name]
          });
        }

        array = helper.refactorOrganizations(daughter, array);
      });
    }

    return array;
  }
};

module.exports = helper;
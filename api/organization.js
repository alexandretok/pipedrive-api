const express = require('express');
const router = express.Router();
const helpers = require('../helpers');
const db = require('../models');

/* GET organization relations */
router.get('/:org_name/:page?', async function (req, res, next) {
  let page = req.params.page | 0;
  const limit = 100;
  const organization = await db.Organization.findOne({
    where: { org_name: req.params.org_name },
    include: ['parents']
  });

  if (!organization) return res.status(404).send();

  // Raw query to retrieve all relations at once for easier pagination
  db.sequelize.query(`
    SELECT org_name, 'parent' as relationship_type FROM Parents INNER JOIN Organizations ON id = parentId WHERE OrganizationId = ${organization.id}
    UNION SELECT org_name, 'daughter' as relationship_type FROM Parents INNER JOIN Organizations ON id = OrganizationId WHERE parentId = ${organization.id}
    ${
      !organization.parents.length ? `` :
      `UNION SELECT org_name, 'sister' as relationship_type FROM Parents INNER JOIN Organizations ON id = OrganizationId WHERE OrganizationId != ${organization.id} AND parentId IN (${organization.parents.map(parent => parent.id)})`
    }
    ORDER BY org_name LIMIT ${page * limit}, ${limit}
  `, {
    model: db.Organization,
    mapToModel: true
  }).then(orgs => {
    res.json(orgs);
  });
});

/* POST new organizations */
router.post('/', function (req, res, next) {
  // Reorganizes the input object
  let organizations = helpers.refactorOrganizations(req.body);

  // Inserts all organizations at once
  db.Organization.bulkCreate(organizations.map(org => ({ org_name: org.org_name })), {
    ignoreDuplicates: true
  });

  // Gets organizations and parents with IDs and creates relationships
  organizations.forEach(async org => {
    let parents = await db.Organization.findAll({
      where: { org_name: org.parents }
    });
    let parentsIds = parents.map(val => val.id);

    org = await db.Organization.findOne({
      where: { org_name: org.org_name }
    });

    // Adds relationship
    parentsIds.forEach(parentId => {
      org.addParents(parentId);
    });
  });

  res.status(201).json({
    processed: organizations.length
  });
});

module.exports = router;
